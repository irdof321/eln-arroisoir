clear;
path = 'data\ConductiveProbe\';
freq = [];
V1=[];
V2=[];
R1=10e3;
legendtxt=[];
figure (1)
figure(2)
for k=0:4
file = [path 'R10k_' num2str(k) '.csv'];
data = readmatrix(file, 'Range', 2);

freq(:,k+1) = data(:,1);
V2(:,k+1) = 10.^(data(:,3)/20).*exp(1j*data(:,4)*pi/180);
Amp2(:,k+1) = 10.^(data(:,3)/20);
Phase2(:,k+1) = data(:,4)*pi/180;
end
Rpr=R1./(1-Amp2.*sqrt(tan(Phase2).^2+1));
R=R1*Rpr./(R1+Rpr);
C=-tan(Phase2)./(2*pi*freq.*R);

Cste = [1e-15,2e10];
w1= [2.489e3,2.489e3];
%w2=[1.5e3,1.5e3];

figure(1)
loglog(freq,Rpr,'Linewidth',3)
hold on;
loglog(w1,Cste,'Linewidth',3,'Color','k')
legend('K=0','K=1','K=2','K=3','K=4')
xlabel('f [Hz]')
ylabel('R [\Omega]')
axis=([min(freq) max(freq) 9.5e3 1e7]);
set(gca,'FontSize',20)
grid on;

figure(2)
loglog(freq,C,'Linewidth',3)
legend('K=0','K=1','K=2','K=3','K=4')
hold on;
loglog(w1,Cste,'Linewidth',3,'Color','k')
xlabel('f [Hz]')
ylabel('C [F]')
set(gca,'FontSize',20)
grid on;

%% LtSpice

dataL=load('LtSpice\Draft3.txt');
t=dataL(:,1);
V2=dataL(:,2);
V1=dataL(:,3);

figure(3)
plot(t,V2,t,V1,'Linewidth',3)
legend('V_2','V_1')
xlim([0 0.0004])
grid on;
set(gca,'FontSize',20)
xlabel('t [s]')
ylabel('U [V]')
